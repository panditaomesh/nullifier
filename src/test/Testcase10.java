package test;
//package InternalTestcases;

/**
 * Tests whether the conditional heuristics are handled.
 * There are no NPE bugs in the program.
 * @author ravi
 *
 */
public class Testcase10 {

	String message;
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Testcase10 A = new Testcase10();
		Testcase10 B = new Testcase10();
		
		A.message = new String(" ");
		B.message = null;
		
		if(args.length > 1)
		{
			B = A;
		}
		
		if(B == A)
		{
			System.out.println(B.message.length());
		}
	}
}

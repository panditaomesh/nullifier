package test;
public class Testcase16{
	/**
	 * @param args
	 * @author omesh : g[1].length() should be error
	 */
	public static void main(String[] args) 
	{
		String[] g = new String[3];
		g[1] = "wo";
		g[2] = "wow";
		if(g[1] != g[2]){
			g[1] = null;
		}
		System.out.println(g[1].length());
		System.out.println(g[2].length());
	}
}

package test;


/**
 * Tests strong update.
 * There is no NPE bug in the program 
 * @author ravi
 *
 */
public class Testcase3 {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {		
		String msg = null;
		
		if(args.length < 1)
		{
			msg = new String("");			
		}
		else
		{
			int i=0;
			do
			{
				msg = args[i];				
				i++;
			}
			while(i < args.length);
		}
		
		System.out.println(msg.length());	
	}
}

package test;


/**
 * Tests weak update.
 * 
 * Bugs:
 * alias.message.length may result in a NPE 
 * @author ravi
 */
public class Testcase6 {

	Word message;
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		Testcase6 t6 = new Testcase6();
		Testcase6 alias = t6;
		t6.message = null;	
				
		if(args.length > 1)
		{
			t6 = new Testcase6();			
		}
				
		t6.message = new Word(" ");
		
		System.out.println(alias.message.val);		
	}

}

class Word {
	String val;
	public Word(String s) {
		val = new String(s);
	}
}


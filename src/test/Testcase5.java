package test;


/**
 * Tests the handling of conditionals.
 * There is no NPE bug in this program.
 * @author ravi
 *
 */
public class Testcase5 {

	String message;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Testcase5 t5 = new Testcase5();
		
		int i=0;
		while(i < args.length)
		{
			t5 = null;
			i++;
		}
		
		if(t5 != null)
			System.out.println(t5.message.length());
	}
}

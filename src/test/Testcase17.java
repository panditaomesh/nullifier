package test;
public class Testcase17{
	/**
	 * @param args
	 * @author omesh : Null Pointer Exception in a.length()
	 */
	public static void main(String[] args) 
	{
		String a = null;
		 int month = 3;
        	switch (month) {
            	case 1:  System.out.println("January"); break;
            	case 2:  System.out.println("February"); break;
            	case 3:  System.out.println(a.length()); break;
            	case 4:  System.out.println("April"); break;
                default: System.out.println("Invalid month.");break;
        }

	}
}

package test;


/**
 * Tests the handling of function calls
 * There is no NPE bug in the program 
 * @author ravi
 */
public class Testcase4 {

	public class A
	{
		public B b;
	}	
	
	public class B
	{
		public String message;
	}
	
	/**
	 * @param args
	 */
	public void init(A a)
	{		
		if(a.b == null)
			a.b = new B();
		else
			a.b.message = new String(" ");
	}
	
	public static void main(String[] args) {

		Testcase4 t4 = new Testcase4();
		A a = t4.new A();
		a.b = null;
		
		t4.init(a);
		
		a.b.message = null;
		
		t4.init(a);
		
		System.out.println(a.b.message.length());
	}
}

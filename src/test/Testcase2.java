package test;


/**
 * Checks the correctness of fixpoint computation, Join operator implementation and also the monotonicity of transfer functions.
 * Bugs:
 * message.length() may result in a NPE
 * @author ravi
 */
public class Testcase2 {		
	
	public static void main(String args[])
	{
		String message = null;		
		
		int i=0;
		while(i < args.length)
		{				 
			message = args[i];
			
			if( (i % 2) == 0 )
			{
				message = new String("");
			}			
		}		
		System.out.println(message.length());
	}
}

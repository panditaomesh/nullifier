package test;
//package testcases;

/**
 * Tests the handling of function calls
 * There is an NPE bug in the program 
 * @author ravi. Modified by Anirudh
 */
public class Testcase11 {

	public class A
	{
		public B b;
		public B second;
	}	
	
	public class B
	{
		public String message;
		public C c;
	}

	public class C
	{
		public String message;
	}
	/**
	 * @param args
	 */
	public void init(A a)
	{		
		/*if(a.b == null)
			a.b = new B();
		else
			a.b.message = new String(" ");*/
	}
	
	public static void main(String[] args) {

		Testcase11 t8 = new Testcase11();
		A a = t8.new A();
		a.b = t8.new B();
		a.b.c = null;
		a.second = null;
		t8 = null;
		t8.init(a);
		
		//a.b.message = null;
		
		//t8.init(a);
		
		System.out.println(a.b.c.message.length());
	}
}

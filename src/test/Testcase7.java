package test;

/**
 * Checks almost all features. 
 * Bugs:
 * head.message.length() may result in a NPE
 * @author ravi
 */
public class Testcase7 {
	
	public String message;
	public Testcase7 field;
	
	public static void main(String args[])
	{
		Testcase7 head = new Testcase7();
		head.message = null;
		head.field = null;
		
		int i=0;
		Testcase7 t7 = head;
		while(i < args.length)
		{	
			t7.field = new Testcase7(); 
			t7.message = args[i];
			t7 = t7.field;
			i++;
		}		
		System.out.println(head.message.length());
	}
}

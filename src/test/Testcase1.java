package test;


/**
 * Basic test. 
 * Bugs:
 * t1.message.length() may result in a NPE 
 * @author ravi
 */
public class Testcase1 {

	public String message;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Testcase1 t1 = new Testcase1();
		
		if(args.length > 1)
		{
			t1.message = args[0];
		}
		else
			t1.message = null;
		
		System.out.println(t1.message.length());
	}
}

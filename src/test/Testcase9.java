package test;
//package InternalTestcases;

/**
 * Tests a subtle condition of access path update where one can do strong update
 * There is no NPE bug in the program 
 * @author ravi
 *
 */
public class Testcase9 {
	
	String message;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Testcase9 t7 = new Testcase9();
		t7.message = null;	
		
		if(args.length < 1)
		{			
			t7.message = new String("");
		}
		else
		{
			int i=0;
			do
			{		
				t7.message = args[i];
				i++;
			}
			while(i < args.length);
		}
		
		System.out.println(t7.message.length());
	}
}

package test;
public class Testcase18{
	/**
	 * @param args
	 * @author omesh : Null Pointer Exception in try block
	 */
	public static void main(String[] args) 
	{
		String a = null;
		 try{
			System.out.println(a.length());
       	 	}
		catch(Exception e){
			a = "wow";
			System.out.println(a.length());
		}


	}
}

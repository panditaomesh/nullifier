package test;
//package InternalTestcases;

/**
 * Tests a specific condition in the handling of function calls.
 * Tests the whether the object on which the method is called is also considered as a parameter.
 * There is no NPE bug in the program 
 * @author ravi
 */
public class Testcase8 {

	public String message;	
	/**
	 * @param args
	 */
	public void foo()
	{
		this.message = new String("Hello World");		
	}
	
	public static void main(String[] args) {
		
		Testcase8 t8 = new Testcase8();
		Testcase8 t7 = new Testcase8();
		t8.message = null;
		t7.message = null;				
		
		t7.foo();
		
		System.out.println(t8.message.length());
		System.out.println(t7.message.length());		
	}
}

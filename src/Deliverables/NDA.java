package Deliverables;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Map;

import soot.Body;
import soot.BodyTransformer;
import soot.PackManager;
import soot.Transform;
import soot.toolkits.graph.BriefUnitGraph;
import soot.toolkits.graph.UnitGraph;


public class NDA {
	static ArrayList<Integer> buggy = new ArrayList<Integer>();
	/**
	 * @param args
	 * <ul>
	 * <li>args[0] - The classpath of the input</li>
	 * <li>args[1] - The class in the source dir that has the main method (needed by soot for constructing call graph)</li>
	 * <li>args[2] - The class that is to be analyzed. All classes that are referenced by this class will also be analyzed</li>
	 * </ul>
	 */
	public static void main(String[] args) {
		if(args.length < 3)
		{
			System.out.println("Insufficient arguments (see the java docs for input format)");
			System.exit(-1);
		}

		String[] soot_args = new String[]{
				"-soot-class-path",
				"" + args[0],
				"-pp",
				"-keep-line-number",
				"-main-class",
				args[1],
				"-p", "cg.spark" ,"verbose:true",
				"-allow-phantom-refs",
				//"-app",
				args[1],
				"-i",
				args[2],
			    "-f",
				"jimple"
		};

		Transform tf = new Transform("jtp.NullDeref", new BodyTransformer() {


			protected void internalTransform(Body b, String phaseName, Map options) {

				if(!phaseName.equals("jtp.NullDeref"))
					return;

				System.out.println("\nAnalyzing method: "+b.getMethod().getName());
				UnitGraph cfg = new BriefUnitGraph(b);
				NullDrefAnalysis ndAnalysis = new NullDrefAnalysis(cfg);
				System.out.println("\nEnd of analysis of method: "+b.getMethod().getName());
				ndAnalysis.printBuggyStatements();
				buggy = NullDrefAnalysis.bugs;

			}

		   });

	   PackManager.v().getPack("jtp").add(tf);

	   soot.Main.main(soot_args);
	   try{
		    args[2] = args[2].replace('.', '/');
			BufferedReader in = new BufferedReader(new FileReader(args[0]+ args[2] + ".java"));
			String line;
			int count = 0;
			while((line = in.readLine()) != null){
				count++;
				if(buggy.contains(Integer.valueOf(count)))
					System.out.println("Line "+count +": "+line);
			}
		}
		catch(Exception e){
			System.out.println("Error printing buggy Statements");
		}
	}
}
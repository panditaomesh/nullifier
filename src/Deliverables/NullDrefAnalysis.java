package Deliverables;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.jimple.*;
import soot.jimple.internal.*;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.NullConstant;
import soot.jimple.ParameterRef;
import soot.jimple.Stmt;
import soot.jimple.ThisRef;
import soot.jimple.internal.JimpleLocal;
import soot.tagkit.LineNumberTag;
import soot.toolkits.graph.UnitGraph;
import soot.toolkits.scalar.ForwardBranchedFlowAnalysis;


public class NullDrefAnalysis extends ForwardBranchedFlowAnalysis<PointstoGraph> {


    /**
     * constructor that accepts a UnitGraph (CFG) and calls the doAnalysis() method
     * @param graph - control flow graph
     */
	public static ArrayList<Integer> bugs = new ArrayList<Integer>();
	private static ArrayList<Integer> checks = new ArrayList<Integer>();
	private static int count = 0;
	private static HashMap<String,Integer> hm = new HashMap<String,Integer>();

    public NullDrefAnalysis(UnitGraph graph) {
	super(graph);
	for(Iterator<Unit> i = graph.iterator();i.hasNext();){
		// Assigns a unique line number to every jimple statement
		Stmt s = (Stmt)i.next();
		LineNumberTag tag = (LineNumberTag)s.getTag("LineNumberTag");
		if(tag != null){
		if(!hm.containsKey(s.toString() + tag.toString())){
			hm.put(s.toString() + tag.toString(), new Integer(count++));
		}
		}
	}
	doAnalysis();
    }

    /**
     * The implementation of transfer function. This should invoke the transfer function corresponding to the input statement
     *
     * @param in (input) input points-to graph (Join of all the predecessor's points-to graphs)
     * @param s (input) The current statement s which can be AssignStmt, IdentityStmt, IfStmt, InvokeStmt etc.
     * @param fallOut (output) a single element List that corresponds to the points-to graph of the fall out branch.
     * @param branchOuts (output) a multi element List of points-to graphs
     *
     */
    protected void flowThrough(PointstoGraph in, Unit s, List<PointstoGraph> fallOut, List<PointstoGraph> branchOuts) {

	/* This method contains skeleton code. You need to fill it in. */

	assert(s instanceof Stmt);
	Stmt stm = (Stmt) s;
	LineNumberTag tag = (LineNumberTag)stm.getTag("LineNumberTag");
	PointstoGraph out = in.clone();
	if(tag != null){
	for( Iterator<Integer> i = checks.iterator();i.hasNext();){
		if(hm.get(stm.toString() + tag.toString()).intValue() > i.next().intValue() && hm.get(stm.toString() + tag.toString()).intValue() < i.next().intValue()){
			if(stm.fallsThrough()){
				mergeWithPointstoGraphs(fallOut,out);
	    	}
	    	if(stm.branches()){
	    		mergeWithPointstoGraphs(branchOuts,out);
	    	}
			return;
		}
	}
	}

	PointerMap pm;
	Collection<SymObj> nr = new HashSet<SymObj>();
    nr.add(SymObj.Null());
    System.out.print("  statement ---> " + stm.toString());
    System.out.print("  type of ---> " + stm.getClass().toString());

	if(stm instanceof JReturnStmt || stm instanceof JReturnVoidStmt){
		if(stm.fallsThrough()){
			mergeWithPointstoGraphs(fallOut,out);
    	}
    	if(stm.branches()){
    		mergeWithPointstoGraphs(branchOuts,out);
    	}
	}
	else if(stm instanceof AssignStmt){
            AssignStmt asStm = (AssignStmt) stm;
            Value left = asStm.getLeftOp();
            Value right = asStm.getRightOp();
            System.out.print("  left type of ---> " + left.getClass().toString());
            System.out.print("  right type of ---> " + right.getClass().toString());
            if(right instanceof JCastExpr){
            	System.out.print("<--- In JCastExpr ---> ");
            	right = ((JCastExpr) right).getOp();
            }


            if((left instanceof JimpleLocal||left instanceof ArrayRef) && right instanceof NullConstant){

            	System.out.print("<--- In 1 ---> ");
				Pointer ptr = getPointerForRef(left); // Gets pointer for jimple local or arraybase
				pm = new PointerMap(ptr,nr); // Creates a pointer map in which pointer points to Null Symoblic object
            	if(left instanceof JimpleLocal)
					clearPointer(out,ptr); // Clears points to set for strong update
            	out.addMap(pm); // Adds the new pointermap to out

            }

            else if( (left instanceof JimpleLocal||left instanceof ArrayRef) &&
		     (right instanceof JimpleLocal||right instanceof ArrayRef) &&
		     (in.getPointstoSet(getPointerForRef(right))!=null)){

				System.out.print("<--- In 2 ---> ");
				Pointer ptr = getPointerForRef(left); // Gets pointer for jimple local or arraybase
				pm = new PointerMap(ptr,out.getPointstoSet(getPointerForRef(right))); // Creates a pointer map in which pointer points either jimple local or arraybase
            	if(left instanceof JimpleLocal)
					clearPointer(out,ptr); // Clears points to set for strong update
            	out.addMap(pm); // Adds the new pointermap to out

			}

            else if(left instanceof FieldRef && right instanceof NullConstant){

            	System.out.print("<--- In 3 ---> ");
				if(left instanceof InstanceFieldRef){

            		InstanceFieldRef ir = (InstanceFieldRef) left; // Typecast the left operator to InstanceFieldRef
            		Pointer ptr = new Pointer(ir.getBase()) ; // Gets pointer for base of fieldref

            		if(containsNull(out,ptr)){
            			// If base pointer points to null,reports the statement as buggy statement
            			System.err.println("Store this statement  " + tag.toString() +" "+stm.toString() );
            			bugs.add(Integer.valueOf(tag.toString()));

            		}
            		if(!(out.getPointstoSet(ptr) == null)){

						if(out.getPointstoSet(ptr).size() == 1 ||(out.getPointstoSet(ptr).size() == 2
            				&& containsNull(out,ptr))){

							// If base pointer points to either single object or two objects but contains null
							for(SymObj so: out.getPointstoSet(ptr)){

								if(!(so.equals(SymObj.Null()))){
									out.setPointstoSet(new Pointer(so,ir.getFieldRef()),nr); // Adds the null pointer to points to set of field
								}
							}

						}
						else{
							for(SymObj so: out.getPointstoSet(ptr)){   // Changed to ptr
            				// Iterate through the points to set of base pointer

            					if(!(so.equals(SymObj.Null()))){

            						Collection<SymObj> ob = new HashSet<SymObj>();
            						ob.add(SymObj.Null());

            						if(out.getPointstoSet(new Pointer(so,ir.getFieldRef())) == null){
            							// If points to set of field is null,then we add a new object to it.

            							SymObj nso = new SymObj();
            							nso.setId(hm.get(stm.toString() + tag.toString()));
            							ob.add(nso);

            						}
            						else{
            							ob.addAll(out.getPointstoSet(new Pointer(so,ir.getFieldRef())));
            						}
            						// Adds the new points to set to out
            						out.setPointstoSet(new Pointer(so,ir.getFieldRef()),ob);

								}
							}
						}
            		}
				}
            }

            else if( (left instanceof FieldRef) &&
		     (right instanceof JimpleLocal || right instanceof ArrayRef )&&
		     (in.getPointstoSet(getPointerForRef(right))!=null)){

            	System.out.print("<--- In 4 ---> ");

            	if(left instanceof InstanceFieldRef){
            		InstanceFieldRef ir = (InstanceFieldRef) left; // Typecast the left operator to InstanceFieldRef
            		Pointer ptr = new Pointer(ir.getBase()) ; // Gets pointer for base of fieldref

            		if(containsNull(out,ptr)){
            			// If base pointer points to null,reports the statement as buggy statement
            			System.err.println("Store this statement2 " + tag.toString() + " " +stm.toString());
            			bugs.add(Integer.valueOf(tag.toString()));
            		}

            		if(!(out.getPointstoSet(ptr) == null)){

            			if(out.getPointstoSet(ptr).size() == 1 ||(out.getPointstoSet(ptr).size() == 2
                				&& containsNull(out,ptr))){
            				// If base pointer points to either single object or two objects but contains null
            				for(SymObj so: out.getPointstoSet(ptr)){

                				if(!(so.equals(SymObj.Null()))){

                					Collection<SymObj> ob = new HashSet<SymObj>();
                					ob.addAll(out.getPointstoSet(getPointerForRef(right))); // Creates a pointer map in which pointer points to pointset either jimple local or arraybase
    								out.setPointstoSet(new Pointer(so,ir.getFieldRef()),ob); // Adds the new points to set to out

                				}
                			}
            			}
            			else{

                			for(SymObj so: out.getPointstoSet(new Pointer(ir.getBase()))){
                				// Iterate through the points to set of base pointer
                					if(!(so.equals(SymObj.Null()))){

                						Collection<SymObj> ob = new HashSet<SymObj>();
                						ob.addAll(out.getPointstoSet(getPointerForRef(right)));
                						if(out.getPointstoSet(new Pointer(so,ir.getFieldRef())) == null){
                							//If points to set of Fieldref is null,then create a new symbolic object and add new symbolic object to points to set of it.
                							SymObj nso = new SymObj();
                							nso.setId(hm.get(stm.toString() + tag.toString()));
                							ob.add(nso);

                						}
                						else{
                							ob.addAll(out.getPointstoSet(new Pointer(so,ir.getFieldRef())));
                						}
                						out.setPointstoSet(new Pointer(so,ir.getFieldRef()),ob); // Adds the new points to set to out
                				}
                			}
                		}
            		}
				}
			}

            else if( (left instanceof JimpleLocal) &&
		     (right instanceof FieldRef )
		     ){System.out.print("<--- In 5 ---> ");
			 	if(right instanceof InstanceFieldRef){
            	  	InstanceFieldRef ir = (InstanceFieldRef) right; // Typecast the left operator to InstanceFieldRef
					Pointer ptr = new Pointer(ir.getBase()) ; // Gets pointer for base of fieldref

        		if(containsNull(out,ptr)){
        			// If base pointer points to null,reports the statement as buggy statement
        			System.err.println("Store this statement3" + tag.toString()+ " " +  stm.toString());
        			bugs.add(Integer.valueOf(tag.toString()));
        		}
        		if(!(out.getPointstoSet(ptr) == null)){
        			for(SymObj so: out.getPointstoSet(ptr)){
        				// Iterate through the points to set of base pointer
        				if(!(so.equals(SymObj.Null()))){

        					Collection<SymObj> ob = new HashSet<SymObj>();
        					Collection<SymObj> ob2 = new HashSet<SymObj>();

        					if(out.getPointstoSet(new Pointer(so,ir.getFieldRef())) == null){
        						//If points to set of Fieldref is null,then create a new symbolic object and add new symbolic object to points to set of field ref.
        						SymObj nso = new SymObj();
        						nso.setId(hm.get(stm.toString() + tag.toString()));
        						ob2.add(nso);
        						out.setPointstoSet(new Pointer(so,ir.getFieldRef()),ob2);
        					}
        					else{
        						if(out.getPointstoSet(new Pointer(so,ir.getFieldRef())) != null)
        							ob.addAll(out.getPointstoSet(new Pointer(so,ir.getFieldRef())));
        					}
        					if(out.getPointstoSet(getPointerForRef(left)) != null){
        						//Adds points set of Jimplelocal
        						ob.addAll(out.getPointstoSet(getPointerForRef(left)));
        					}

        					out.setPointstoSet(new Pointer(left),ob); // Adds the new points to set to out

        					}
        				}
        			}
            	}
            	else{
              		Collection<SymObj> s1 = new HashSet<SymObj>();  //??
            		SymObj s2 = new SymObj();
            		s2.setId(hm.get(stm.toString() + tag.toString()));
            		s1.add(s2);
            		out.setPointstoSet(new Pointer(left), s1);
            	}
			}
            else if( (left instanceof JimpleLocal) &&
		     (right instanceof AnyNewExpr || right instanceof StringConstant || right instanceof ClassConstant)
		     ){

            	System.out.print("<--- In 6 ---> ");
				clearPointer(out,new Pointer(left)); // Clears the points to set for strong update
            	Collection<SymObj> no = new HashSet<SymObj>(); // Creates collection of new symbolic object
            	SymObj so = new SymObj();
            	so.setId(hm.get(stm.toString() + tag.toString()));
            	no.add(so);
            	out.setPointstoSet(new Pointer(left),no); // Adds the new points to set to out

			}
            else if( (left instanceof InstanceFieldRef) &&
		     (right instanceof AnyNewExpr || right instanceof StringConstant || right instanceof ClassConstant )
		     ){

            	System.out.print("<--- In 7 ---> ");
            	if(left instanceof InstanceFieldRef){
            		InstanceFieldRef ir = (InstanceFieldRef) left; // Typecast the left operator to InstanceFieldRef
            		Pointer ptr = new Pointer(ir.getBase()) ; // Gets pointer for base of fieldref

            		if(containsNull(out,ptr)){
            			// If base pointer points to null,reports the statement as buggy statement
            			System.err.println("Store this statement  " + tag.toString() +" " + stm.toString() );
            			bugs.add(Integer.valueOf(tag.toString()));
            		}

            		if(!(out.getPointstoSet(ptr) == null)){

            			if(out.getPointstoSet(ptr).size() == 1 ||(out.getPointstoSet(ptr).size() == 2
            					&& containsNull(out,ptr))){

            				// If base pointer points to either single object or two objects but contains null
            				for(SymObj so: out.getPointstoSet(ptr)){

            					if(!(so.equals(SymObj.Null()))){

            						Collection<SymObj> ob = new HashSet<SymObj>(); // Creates collection of new symbolic object
            						SymObj nso = new SymObj();
            						nso.setId(hm.get(stm.toString() + tag.toString()));
            						ob.add(nso);
            						out.setPointstoSet(new Pointer(so,ir.getFieldRef()),ob); // Adds the new points to set of fieldref to out
            					}
            				}
            			}
            			else{
            			//Weak update
            				for(SymObj so: out.getPointstoSet(ptr)){
        					// Iterate through the points to set of base pointer
            					if(!(so.equals(SymObj.Null()))){
            						Collection<SymObj> ob = new HashSet<SymObj>();
            						if(out.getPointstoSet(new Pointer(so,ir.getFieldRef())) == null){
            						// If points to set of field is null,then create new object and add it.
            							SymObj nso = new SymObj();
            							nso.setId(hm.get(stm.toString() + tag.toString()));
            							ob.add(nso);
            						}
            						else{
        							//Otherwise add the existing points to set of field ref
            							ob.addAll(out.getPointstoSet(new Pointer(so,ir.getFieldRef())));
            						}
            						// Create the new symbolic object for new expression
            						SymObj nso2 = new SymObj();
            						nso2.setId(hm.get(stm.toString() + tag.toString()));
            						ob.add(nso2);
            						out.setPointstoSet(new Pointer(so,ir.getFieldRef()),ob); // Adds the new points to set of fieldref to out

            					}
            				}
            			}
            		}
            	}
            	else{
            		// If points to set of base pointer is empty,then create a new symbolic object and add it points set of base pointer
            		Collection<SymObj> s1 = new HashSet<SymObj>();
            		SymObj s2 = new SymObj();
            		s2.setId(hm.get(stm.toString() + tag.toString()));
            		s1.add(s2);
            		out.setPointstoSet(new Pointer(left), s1);
           		}
            }

            else if( (left instanceof JimpleLocal) &&
		     (right instanceof InvokeExpr)
		     ){System.out.print("<--- In 8 ---> ");
            	if(right instanceof StaticInvokeExpr){

            		Collection<SymObj> s1 = new HashSet<SymObj>(); // Creates collection of new symbolic object
            		SymObj s2 = new SymObj();
            		s2.setId(hm.get(stm.toString() + tag.toString()));
            		s1.add(s2);
            		out.setPointstoSet(new Pointer(left), s1); // Adds the new points to set of fieldref to out

            	}
            	else{
            	 	InstanceInvokeExpr ink = (InstanceInvokeExpr) right; // Typecast the left operator to InstanceFieldRef
                	Value j = ink.getBase(); // Gets pointer for base of fieldref
                	Pointer ptr = new Pointer(j) ;

            		if(containsNull(out,ptr)){
            			// If base pointer points to null,reports the statement as buggy statement
            			System.err.println("Store this statement4  " + tag.toString()+" "+ stm.toString() );
            			bugs.add(Integer.valueOf(tag.toString()));
            		}

            		Collection<SymObj> s1 = new HashSet<SymObj>(); // Typecast the left operator to InstanceFieldRef
            		SymObj s2 = new SymObj();
            		s2.setId(hm.get(stm.toString() + tag.toString()));
            		s1.add(s2);
            		out.setPointstoSet(new Pointer(left), s1); // Adds the new points to set of fieldref to out
            	}
			}
			if(stm.fallsThrough()){
				mergeWithPointstoGraphs(fallOut,out);
        	}
        	if(stm.branches()){
        		mergeWithPointstoGraphs(branchOuts,out);
        	}
        	System.out.print("  In is ---> ");
        	in.disp();
        	System.out.print("  Outset is ---> ");
        	out.disp();
        	System.out.println("");
	}
	else if(stm instanceof InvokeStmt){
		InvokeStmt ink = (InvokeStmt) stm;
		Collection<Pointer> values = new HashSet<Pointer>();
		Collection<SymObj> obj = new HashSet<SymObj>();
		Collection<SymObj> obj2 = new HashSet<SymObj>();

		Value v1 = ink.getUseBoxes().get(0).getValue();//get base of invoke statement
		for(ListIterator<ValueBox> k = ink.getUseBoxes().listIterator();k.hasNext();){//fetch the parameters of invoke statement
			ValueBox v2 = (ValueBox)k.next();
			values.add(new Pointer(v2.getValue()));//add pointers to values set
		}
		for(Pointer i : values){//for pointer values get symbolic objects the pointer is pointing to and add to obj
			if(out.getPointstoSet(i) != null){
			for(SymObj j : out.getPointstoSet(i)){
				if(j != SymObj.Null()){
					obj.add(j);
					}
				}
			}
		}
		obj2.addAll(obj);//add all elements of obj to obj2
		while(true){//again search for all accesible symbolic objects form obj2
		for(PointerMap i : out.getPointerMaps()){
			if(obj.contains(i.getPointer().symObj)){
				for(SymObj j:i.getMapsToSet()){
					if(j != SymObj.Null()){
						obj2.add(j);
					}
				}
			}
		}
			if(obj.equals(obj2)) break;//if no more symbolic objects are reachable break
			else obj = obj2;
		}
		for(PointerMap i : out.getPointerMaps()){
				if(obj.contains(i.getPointer().symObj)){
					i.getPointstoSet().remove(SymObj.Null());// remove null form all accessible pointer refrences.
					if(i.getPointstoSet().isEmpty()){//if points to set becomes null for some pointer create a new symbolic object and add to it's points to set
						SymObj s1 = new SymObj();
						s1.setId(hm.get(stm.toString() + tag.toString()));
						Collection<SymObj> col = new HashSet<SymObj>();
						col.add(s1);
						i.setMapsTo(col);
					}
				}
			}

		Pointer ptr = new Pointer(v1) ;//create pointer for base

		if(containsNull(out,ptr)){//if base contains null it can be a Null dereference error
			System.out.println("Store this statement4  " + tag.toString() + " "+stm.toString() );
			bugs.add(Integer.valueOf(tag.toString()));
		}

		if(stm.fallsThrough()){//merge out with points to graph in fallout
				mergeWithPointstoGraphs(fallOut,out);
			}
		if(stm.branches()){//merge out with points to graphs in branch out
				mergeWithPointstoGraphs(branchOuts,out);
		}
		System.out.print("  Outset is ---> ");
        	out.disp();
        	System.out.println("");
	}//end of invoke stmt if

	else if(stm instanceof IdentityStmt){
	    IdentityStmt idStm = (IdentityStmt) stm;
	    Value left = idStm.getLeftOp();
	    Value right = idStm.getRightOp();


	    if( (left instanceof JimpleLocal) &&
		(right instanceof ParameterRef || right instanceof ThisRef)
		){
			clearPointer(out, new Pointer(left));  // Clears points to set for strong update
			Collection<SymObj> s1 = new HashSet<SymObj>(); // Creates collection of new symbolic object
			SymObj s2 = new SymObj();
			s2.setId(hm.get(stm.toString() + tag.toString()));
			s1.add(s2);
			out.setPointstoSet(new Pointer(left), s1); // Adds the new points to out
			if(stm.fallsThrough()){
				mergeWithPointstoGraphs(fallOut,out);
			}
			if(stm.branches()){
				mergeWithPointstoGraphs(branchOuts,out);
			}
	    }
	    System.out.print("  Outset is ---> ");
    	out.disp();
    	System.out.println("");
	}

	else if(stm instanceof JIfStmt){
	    JIfStmt ifStm = (JIfStmt) stm;
	    Value condVal = ifStm.getCondition();
		PointstoGraph fout = out.clone(); // Creates points to graph for fall out
		PointstoGraph bout = out.clone(); // Creates points to graph for branch out
	    if (condVal instanceof  JInstanceOfExpr) {
	    	for(PointerMap p :fout.getPointerMaps()){
				if(p.getPointstoSet().contains(SymObj.Null())){
					p.getPointstoSet().remove(SymObj.Null());
					if(p.getPointstoSet().size() == 0){
						Collection<SymObj> s1 = new HashSet<SymObj>();
	            		SymObj s2 = new SymObj();
	            		s2.setId(hm.get(stm.toString() + tag.toString()));
	            		s1.add(s2);
	            		fout.setPointstoSet(p.getPointer(), s1);
					}
				}
			}
	    }
	    else if(condVal instanceof EqExpr){
		EqExpr eq = (EqExpr) condVal;
		Value left = eq.getOp1(); // Gets left operand
		Value right = eq.getOp2(); // Gets right operand
		if((left instanceof NullConstant)&&
		   (right instanceof JimpleLocal || right instanceof ArrayRef)
		   ){

			if(!out.getPointstoSet(getPointerForRef(right)).contains(SymObj.Null())){
				// If points to set of jimplelocal or arrayref doesn't contain null,then true path won't be feasible.
				for(PointerMap p :fout.getPointerMaps()){
					if(p.getPointstoSet().contains(SymObj.Null())){
						p.getPointstoSet().remove(SymObj.Null()); // So we remove null from points to graph for the infeasible path
						if(p.getPointstoSet().size() == 0){
							// After removing null ,if the points to graph is null,we add a new symbolic object.
							Collection<SymObj> s1 = new HashSet<SymObj>();
		            		SymObj s2 = new SymObj();
		            		s2.setId(hm.get(stm.toString() + tag.toString()));
		            		s1.add(s2);
		            		fout.setPointstoSet(p.getPointer(), s1); // Adds the new points to fallout
						}
					}
				}

			}
			else{
			if(out.getPointstoSet(getPointerForRef(right)).contains(SymObj.Null()) &&
					out.getPointstoSet(getPointerForRef(right)).size() == 1	){
				// If true branch is executed and pointer points to only null,then we can skip the statements between condition and goto statement
				bout = new PointstoGraph();
				Stmt gstm = (Stmt)stm.getUnitBoxes().get(0).getUnit(); // Fetch the goto statement
				LineNumberTag gtag = (LineNumberTag)gstm.getTag("LineNumberTag"); // Get the line number of goto statement
				if(hm.get(stm.toString() + tag.toString()).intValue() < hm.get(gstm.toString() + gtag.toString()).intValue()){
				// The statements between the if and goto statements should not be executed,so we keep track of line numbers of if and goto statement.
					checks.add(Integer.valueOf(hm.get(stm.toString() + tag.toString()).intValue()));
					checks.add(Integer.valueOf(hm.get(gstm.toString() + gtag.toString()).intValue()));

				}
				else{

					checks.add(Integer.valueOf(hm.get(gstm.toString() + gtag.toString()).intValue()));
					checks.add(Integer.valueOf(hm.get(stm.toString() + tag.toString()).intValue()));
				}
			}
			else{
				if(right instanceof JimpleLocal){
					// If true branch is executing and size of points to set is greater than one and contains null,then we have execute both the branches
					strongUpdate(fout,getPointerForRef(right),nr); //For true condition,we do strong update and update the points to set null
					bout.getPointstoSet(getPointerForRef(right)).remove(SymObj.Null()); // For false condition,we remove null from the points to set

					}
				}
			}
		}
		else if((right instanceof NullConstant)&&
			(left instanceof JimpleLocal || left instanceof ArrayRef)
			){
			if(out.getPointstoSet(getPointerForRef(left)) != null){
			if(!out.getPointstoSet(getPointerForRef(left)).contains(SymObj.Null())){
				// If points to set of jimplelocal or arrayref doesn't contain null,then true path won't be feasible.
				for(PointerMap p :fout.getPointerMaps()){
					if(p.getPointstoSet().contains(SymObj.Null())){
						// So we remove null from points to graph for the infeasible path
						p.getPointstoSet().remove(SymObj.Null());
						if(p.getPointstoSet().size() == 0){
							// After removing null ,if the points to graph is null,we add a new symbolic object.
							Collection<SymObj> s1 = new HashSet<SymObj>();
		            		SymObj s2 = new SymObj();
		            		s2.setId(hm.get(stm.toString() + tag.toString()));
		            		s1.add(s2);
		            		fout.setPointstoSet(p.getPointer(), s1); // Adds the new points to fallout
						}
					}
				}

			}
			else{
			if(out.getPointstoSet(getPointerForRef(left)).contains(SymObj.Null()) &&
					out.getPointstoSet(getPointerForRef(left)).size() == 1	){
				// If true branch is executed and pointer points to only null,then we can skip the statements between condition and goto statement
				bout = new PointstoGraph();
				Stmt gstm = (Stmt)stm.getUnitBoxes().get(0).getUnit(); // Fetch the goto statement
				LineNumberTag gtag = (LineNumberTag)gstm.getTag("LineNumberTag"); // Get the line number of goto statement
				if(hm.get(stm.toString() + tag.toString()).intValue() < hm.get(gstm.toString() + gtag.toString()).intValue()){
					// The statements between the if and goto statements should not be executed,so we keep track of line numbers of if and goto statement.
					checks.add(Integer.valueOf(hm.get(stm.toString() + tag.toString()).intValue()));
					checks.add(Integer.valueOf(hm.get(gstm.toString() + gtag.toString()).intValue()));
				}
				else{

					checks.add(Integer.valueOf(hm.get(gstm.toString() + gtag.toString()).intValue()));
					checks.add(Integer.valueOf(hm.get(stm.toString() + tag.toString()).intValue()));
				}
			}
			else{
				if(left instanceof JimpleLocal){
					// If true branch is executing and size of points to set is greater than one and contains null,then we have execute both the branches
					strongUpdate(fout,getPointerForRef(left),nr);  //For true condition,we do strong update and update the points to set null
					bout.getPointstoSet(getPointerForRef(left)).remove(SymObj.Null()); // For false condition,we remove null from the points to set
				}
			}
			}
			}
		}
		else if((left instanceof NullConstant)&&
			(right instanceof FieldRef)
			){
			InstanceFieldRef ir = (InstanceFieldRef) right; // Typecast the right operator to InstanceFieldRef
			Pointer ptr = new Pointer(ir.getBase()); // Gets pointer for base of fieldref

    		if(containsNull(out,ptr)){
    			// If base pointer points to null,reports the statement as buggy statement
    			System.err.println("Store this statement3" + tag.toString() + " " +stm.toString());
    			bugs.add(Integer.valueOf(tag.toString()));
    		}

    		if(!(out.getPointstoSet(ptr) == null)){

    			if(out.getPointstoSet(ptr).size() == 1 ||(out.getPointstoSet(ptr).size() == 2
        				&& containsNull(out,ptr))){
    				// If base pointer points to either single object or two objects but contains null
        			for(SymObj so: out.getPointstoSet(ptr)){
        				// Iterate throught points to set of base pointer
        				if(!(so.equals(SymObj.Null()))){
        					clearPointer(fout, new Pointer(so,ir.getFieldRef())); //Clears points to set of field for strong update
        					SymObj s1 = SymObj.Null(); // Create collection for null object
        					Collection<SymObj> s2 = new HashSet<SymObj>();
        					s2.add(s1);
        					fout.setPointstoSet(new Pointer(so,ir.getFieldRef()),s2); // For true condition,we make strong updation such that field pointer points to null only

        					if(bout.getPointstoSet(new Pointer(so,ir.getFieldRef())) == null ||( bout.getPointstoSet(new Pointer(so,ir.getFieldRef())).size() == 1 && bout.getPointstoSet(new Pointer(so,ir.getFieldRef())).contains(SymObj.Null()))){
        						// If points to set is empty or contains null only,then we add new object
        						SymObj nso = new SymObj();
    							nso.setId(hm.get(stm.toString() + tag.toString()));
    							Collection<SymObj> s3 = new HashSet<SymObj>();
            					s3.add(nso);
    							bout.setPointstoSet(new Pointer(so,ir.getFieldRef()), s3);
    						}
        					bout.getPointstoSet(new Pointer(so,ir.getFieldRef())).remove(s1); // We remove null from false branch

        				}
        			}
    			}
    			else{
    				for(SymObj so: out.getPointstoSet(ptr)){
    					// Iterate throught points to set of base pointer
        				if(!(so.equals(SymObj.Null()))){
        					SymObj s1 = SymObj.Null();// Create collection for null object
        					Collection<SymObj> s2 = new HashSet<SymObj>();
        					s2.add(s1);
        					if(out.getPointstoSet(new Pointer(so,ir.getFieldRef())) == null){
        						// If fieldref points to empty,we add a new object
    							SymObj nso = new SymObj();
    							nso.setId(hm.get(stm.toString() + tag.toString()));
    							s2.add(nso);
    						}
        					s2.addAll(out.getPointstoSet(ptr));
        					fout.setPointstoSet(new Pointer(so,ir.getFieldRef()),s2); // We add null to fallout
        					if(bout.getPointstoSet(new Pointer(so,ir.getFieldRef())) == null ||( bout.getPointstoSet(new Pointer(so,ir.getFieldRef())).size() == 1 && bout.getPointstoSet(new Pointer(so,ir.getFieldRef())).contains(SymObj.Null()))){//kuch galat lag raha
        						// If points to set is empty or contains null only,then we add new object
        						SymObj nso = new SymObj();
    							nso.setId(hm.get(stm.toString() + tag.toString()));
    							Collection<SymObj> s3 = new HashSet<SymObj>();
            					s3.add(nso);
    							bout.setPointstoSet(new Pointer(so,ir.getFieldRef()), s3);
    						}
        					bout.getPointstoSet(new Pointer(so,ir.getFieldRef())).remove(s1); // We remove null from false branch
        				}
    				}
    			}
    		}

		}
		else if((right instanceof NullConstant)&&
			(left instanceof FieldRef)
			){
			InstanceFieldRef ir = (InstanceFieldRef) left; // Typecast the left operator to InstanceFieldRef
			Pointer ptr = new Pointer(ir.getBase()); // Gets pointer for base of fieldref

    		if(containsNull(out,ptr)){
    			// If base pointer points to null,reports the statement as buggy statement
    			System.err.println("Store this statement3" + tag.toString() + " "+stm.toString());
    			bugs.add(Integer.valueOf(tag.toString()));
    		}
    		if(!(out.getPointstoSet(ptr) == null)){
    			if(out.getPointstoSet(ptr).size() == 1 ||(out.getPointstoSet(ptr).size() == 2
        				&& containsNull(out,ptr))){
    				// If base pointer points to either single object or two objects but contains null
        			for(SymObj so: out.getPointstoSet(ptr)){
        				// Iterate throught points to set of base pointer
        				if(!(so.equals(SymObj.Null()))){
        					clearPointer(fout, new Pointer(so,ir.getFieldRef()));  //Clears points to set of field for strong update
        					SymObj s1 = SymObj.Null(); // Create collection for null object
        					Collection<SymObj> s2 = new HashSet<SymObj>();
        					s2.add(s1);
        					fout.setPointstoSet(new Pointer(so,ir.getFieldRef()),s2); // For true condition,we make strong updation such that field pointer points to null only
        					if(bout.getPointstoSet(new Pointer(so,ir.getFieldRef())) == null ||( bout.getPointstoSet(new Pointer(so,ir.getFieldRef())).size() == 1 && bout.getPointstoSet(new Pointer(so,ir.getFieldRef())).contains(SymObj.Null()))){//kuch galat lag raha
        						// If points to set is empty or contains null only,then we add new object
        						SymObj nso = new SymObj();
    							nso.setId(hm.get(stm.toString() + tag.toString()));
    							Collection<SymObj> s3 = new HashSet<SymObj>();
            					s3.add(nso);
    							bout.setPointstoSet(new Pointer(so,ir.getFieldRef()), s3);
    						}
        					bout.getPointstoSet(new Pointer(so,ir.getFieldRef())).remove(s1); // We remove null from false branch
        				}
        			}
    			}
    			else{
    				for(SymObj so: out.getPointstoSet(ptr)){
    					// Iterate throught points to set of base pointer
        				if(!(so.equals(SymObj.Null()))){
        					SymObj s1 = SymObj.Null(); // Create collection for null object
        					Collection<SymObj> s2 = new HashSet<SymObj>();
        					s2.add(s1);
        					if(out.getPointstoSet(new Pointer(so,ir.getFieldRef())) == null){
        						// If fieldref points to empty,we add a new object
    							SymObj nso = new SymObj();
    							nso.setId(hm.get(stm.toString() + tag.toString()));
    							s2.add(nso);
    						}
        					s2.addAll(out.getPointstoSet(ptr));
        					fout.setPointstoSet(new Pointer(so,ir.getFieldRef()),s2); // We add null to fallout
        					if(bout.getPointstoSet(new Pointer(so,ir.getFieldRef())) == null ||( bout.getPointstoSet(new Pointer(so,ir.getFieldRef())).size() == 1 && bout.getPointstoSet(new Pointer(so,ir.getFieldRef())).contains(SymObj.Null()))){
        						// If points to set is empty or contains null only,then we add new object
        						SymObj nso = new SymObj();
    							nso.setId(hm.get(stm.toString() + tag.toString()));
    							Collection<SymObj> s3 = new HashSet<SymObj>();
            					s3.add(nso);
    							bout.setPointstoSet(new Pointer(so,ir.getFieldRef()), s3);
    						}
        					bout.getPointstoSet(new Pointer(so,ir.getFieldRef())).remove(s1); // We remove null from false branch

        				}
    				}
    			}
    		}
		}
		else if(left instanceof JimpleLocal && right instanceof JimpleLocal){
			Collection<SymObj> s1 = out.getPointstoSet(getPointerForRef(left));
			Collection<SymObj> s2 = out.getPointstoSet(getPointerForRef(right));
			Collection<SymObj> s3 = new HashSet<SymObj>();

			if(s1 != null && s2!= null){

			// First we take intersection of points to graph of two jimple local
				for(SymObj i : s1){

					if(s2.contains(i)){

						s3.add(i);
					}
				}
				if(s3.size() == 0){
				// If intersection is empty,then true branch will be infeasible so we remove null from it.
					for(PointerMap p :fout.getPointerMaps()){

						if(p.getPointstoSet().contains(SymObj.Null())){
							p.getPointstoSet().remove(SymObj.Null());
							if(p.getPointstoSet().size() == 0){
								Collection<SymObj> s11 = new HashSet<SymObj>();
								SymObj s22 = new SymObj();
								s22.setId(hm.get(stm.toString() + tag.toString()));
								s11.add(s22);
								fout.setPointstoSet(p.getPointer(), s11);
							}
						}
					}
				}

				else{
					if(s1.size() == 1 && s2.size() == 1 && s3.size() == 1){
				//If intersection is same as points to graph of both and size is one
						bout = new PointstoGraph();
						Stmt gstm = (Stmt)stm.getUnitBoxes().get(0).getUnit(); // Fetch the goto statement
						LineNumberTag gtag = (LineNumberTag)gstm.getTag("LineNumberTag"); // Get the line number of goto statement
						if(hm.get(stm.toString() + tag.toString()).intValue() < hm.get(gstm.toString() + gtag.toString()).intValue()){
						// The statements between the if and goto statements should not be executed,so we keep track of line numbers of if and goto statement.
							checks.add(Integer.valueOf(hm.get(stm.toString() + tag.toString()).intValue()));
							checks.add(Integer.valueOf(hm.get(gstm.toString() + gtag.toString()).intValue()));
						}

						else{

							checks.add(Integer.valueOf(hm.get(gstm.toString() + gtag.toString()).intValue()));
							checks.add(Integer.valueOf(hm.get(stm.toString() + tag.toString()).intValue()));
						}
					}
					else{ // Otherwise we change points to set of fall out
						fout.setPointstoSet(getPointerForRef(left),s3);
						fout.setPointstoSet(getPointerForRef(right),s3);
					}
				}
			}
		}
		else{
		}
        }

	    else if(condVal instanceof NeExpr){// doubt
		NeExpr ne = (NeExpr) condVal;
		Value left = ne.getOp1(); // Gets left operand
		Value right = ne.getOp2(); // Gets right operand
		if(!(left instanceof NullConstant) && !(right instanceof NullConstant)){
			Collection<SymObj> s1 = out.getPointstoSet(getPointerForRef(left));
			Collection<SymObj> s2 = out.getPointstoSet(getPointerForRef(right));
			Collection<SymObj> s3 = new HashSet<SymObj>();
			System.out.print("  left type --->  " + left.getType().toString() );
			if(s1 != null && s2!= null){

				// First we take intersection of points to graph of two jimple local
				for(SymObj i : s1){

					if(s2.contains(i)){

						s3.add(i);
					}
				}
				if(s3.size() == 0){
					// If intersection is empty,then false branch will be infeasible
					bout = new PointstoGraph();
					Stmt gstm = (Stmt)stm.getUnitBoxes().get(0).getUnit();
					LineNumberTag gtag = (LineNumberTag)gstm.getTag("LineNumberTag");
					if(hm.get(stm.toString() + tag.toString()).intValue() < hm.get(gstm.toString() + gtag.toString()).intValue()){
						// The statements between the if and goto statements should not be executed,so we keep track of line numbers of if and goto statement.
						checks.add(Integer.valueOf(hm.get(stm.toString() + tag.toString()).intValue()));
						checks.add(Integer.valueOf(hm.get(gstm.toString() + gtag.toString()).intValue()));
					}

					else{

						checks.add(Integer.valueOf(hm.get(gstm.toString() + gtag.toString()).intValue()));
						checks.add(Integer.valueOf(hm.get(stm.toString() + tag.toString()).intValue()));
					}
				}
				else{
					if(s1.size() == 1 && s2.size() == 1 && s3.size() == 1){
						//If intersection is same as points to graph of both and size is one,then true branch will be infeasible so we remove null from it
						for(PointerMap p :fout.getPointerMaps()){
							if(p.getPointstoSet().contains(SymObj.Null())){
								p.getPointstoSet().remove(SymObj.Null());// So we remove null from points to graph for the infeasible path
								if(p.getPointstoSet().size() == 0){
									// After removing null ,if the points to graph is null,we add a new symbolic object.
									Collection<SymObj> s11 = new HashSet<SymObj>();
									SymObj s22 = new SymObj();
									s22.setId(hm.get(stm.toString() + tag.toString()));
									s11.add(s22);
									fout.setPointstoSet(p.getPointer(), s11);
								}
							}
						}
					}
					else{ // Otherwise we change points to set of fall out and branch out both
						bout.setPointstoSet(getPointerForRef(left),s3);
						bout.setPointstoSet(getPointerForRef(right),s3);
					}
				}
			}
		}
		else if((left instanceof NullConstant)&&
			(right instanceof JimpleLocal || right instanceof ArrayRef)
			){
			if(!out.getPointstoSet(getPointerForRef(right)).contains(SymObj.Null())){
				// If points to set of jimplelocal or arrayref doesn't contain null,then false path won't be feasible,so then we can skip the statements between condition and goto statement
				bout = new PointstoGraph();
				Stmt gstm = (Stmt)stm.getUnitBoxes().get(0).getUnit();
				LineNumberTag gtag = (LineNumberTag)gstm.getTag("LineNumberTag");
				if(hm.get(stm.toString() + tag.toString()).intValue() < hm.get(gstm.toString() + gtag.toString()).intValue()){
					// The statements between the if and goto statements should not be executed,so we keep track of line numbers of if and goto statement.
					checks.add(Integer.valueOf(hm.get(stm.toString() + tag.toString()).intValue()));
					checks.add(Integer.valueOf(hm.get(gstm.toString() + gtag.toString()).intValue()));
				}
				else{

					checks.add(Integer.valueOf(hm.get(gstm.toString() + gtag.toString()).intValue()));
					checks.add(Integer.valueOf(hm.get(stm.toString() + tag.toString()).intValue()));
				}
			}
			else{
			if(out.getPointstoSet(getPointerForRef(right)).contains(SymObj.Null()) &&
					out.getPointstoSet(getPointerForRef(right)).size() == 1	){
				// If points to set of jimplelocal or arrayref contains null,then true path won't be feasible.
				for(PointerMap p :fout.getPointerMaps()){
					if(p.getPointstoSet().contains(SymObj.Null())){
						p.getPointstoSet().remove(SymObj.Null()); // So we remove null from points to graph for the infeasible path
						if(p.getPointstoSet().size() == 0){
							// After removing null ,if the points to graph is null,we add a new symbolic object.
							Collection<SymObj> s1 = new HashSet<SymObj>();
		            		SymObj s2 = new SymObj();
		            		s2.setId(hm.get(stm.toString() + tag.toString()));
		            		s1.add(s2);
		            		fout.setPointstoSet(p.getPointer(), s1); // Adds the new points to fallout
						}
					}
				}
			}
			else{
				if(right instanceof JimpleLocal){
					// If false branch is executing and size of points to set is greater than one and contains null,then we have to execute both the branches
					strongUpdate(bout,getPointerForRef(right),nr); //For false condition,we do strong update and update the points to set null
					fout.getPointstoSet(getPointerForRef(right)).remove(SymObj.Null()); // For true condition,we remove null from the points to set
				}
			}
			}
		}
		else if((right instanceof NullConstant)&&
			(left instanceof JimpleLocal || left instanceof ArrayRef)
			){
			if(out.getPointstoSet(getPointerForRef(left)) != null){
				// If points to set of jimplelocal or arrayref doesn't contain null,then false path won't be feasible,so then we can skip the statements between condition and goto statement
			if(!out.getPointstoSet(getPointerForRef(left)).contains(SymObj.Null())){
				bout = new PointstoGraph();
				Stmt gstm = (Stmt)stm.getUnitBoxes().get(0).getUnit();

				LineNumberTag gtag = (LineNumberTag)gstm.getTag("LineNumberTag");
				if(hm.get(stm.toString() + tag.toString()).intValue() < hm.get(gstm.toString() + gtag.toString()).intValue()){
					// The statements between the if and goto statements should not be executed,so we keep track of line numbers of if and goto statement.
					checks.add(Integer.valueOf(hm.get(stm.toString() + tag.toString()).intValue()));
					checks.add(Integer.valueOf(hm.get(gstm.toString() + gtag.toString()).intValue()));
				}
				else{

					checks.add(Integer.valueOf(hm.get(gstm.toString() + gtag.toString()).intValue()));
					checks.add(Integer.valueOf(hm.get(stm.toString() + tag.toString()).intValue()));
				}
			}
			else{
				if(out.getPointstoSet(getPointerForRef(left)).contains(SymObj.Null()) &&
						out.getPointstoSet(getPointerForRef(left)).size() == 1	){
					// If points to set of jimplelocal or arrayref contains null,then true path won't be feasible.
						for(PointerMap p :fout.getPointerMaps()){
							if(p.getPointstoSet().contains(SymObj.Null())){
								p.getPointstoSet().remove(SymObj.Null()); // So we remove null from points to graph for the infeasible path
								if(p.getPointstoSet().size() == 0){
									// After removing null ,if the points to graph is null,we add a new symbolic object.
									Collection<SymObj> s1 = new HashSet<SymObj>();
				            		SymObj s2 = new SymObj();
				            		s2.setId(hm.get(stm.toString() + tag.toString()));
				            		s1.add(s2);
				            		fout.setPointstoSet(p.getPointer(), s1);
								}
							}
						}
					}
				else{
					if(left instanceof JimpleLocal){
						// If false branch is executing and size of points to set is greater than one and contains null,then we have to execute both the branches
						strongUpdate(bout,getPointerForRef(left),nr); //For false condition,we do strong update and update the points to set null
						fout.getPointstoSet(getPointerForRef(left)).remove(SymObj.Null()); // For true condition,we remove null from the points to set
					}
				}
			}
			}
		}
		else if((left instanceof NullConstant)&&
			(right instanceof FieldRef)
			){
			InstanceFieldRef ir = (InstanceFieldRef) right;  // Typecast the right operator to InstanceFieldRef
			Pointer ptr = new Pointer(ir.getBase()); // Gets pointer for base of fieldref

    		if(containsNull(out,ptr)){
    			// If base pointer points to null,reports the statement as buggy statement
    			System.err.println("Store this statement3" + tag.toString() + " " + stm.toString());
    			bugs.add(Integer.valueOf(tag.toString()));
    		}
    		if(!(out.getPointstoSet(ptr) == null)){
    			if(out.getPointstoSet(ptr).size() == 1 ||(out.getPointstoSet(ptr).size() == 2
        				&& containsNull(out,ptr))){
    				// If base pointer points to either single object or two objects but contains null
        			for(SymObj so: out.getPointstoSet(ptr)){
        				// Iterate throught points to set of base pointer
        				if(!(so.equals(SymObj.Null()))){
        					clearPointer(bout, new Pointer(so,ir.getFieldRef())); //Clears points to set of field for strong update
        					SymObj s1 = SymObj.Null();
        					Collection<SymObj> s2 = new HashSet<SymObj>();
        					s2.add(s1);
        					bout.setPointstoSet(new Pointer(so,ir.getFieldRef()),s2); // For false condition,we make strong updation such that field pointer points to null only
        					if(fout.getPointstoSet(new Pointer(so,ir.getFieldRef())) == null ||( fout.getPointstoSet(new Pointer(so,ir.getFieldRef())).size() == 1 && fout.getPointstoSet(new Pointer(so,ir.getFieldRef())).contains(SymObj.Null()))){
        						// If points to set is empty or contains null only,then we add new object
        						SymObj nso = new SymObj();
    							nso.setId(hm.get(stm.toString() + tag.toString()));
    							Collection<SymObj> s3 = new HashSet<SymObj>();
            					s3.add(nso);
    							fout.setPointstoSet(new Pointer(so,ir.getFieldRef()), s3);
    						}
        					fout.getPointstoSet(new Pointer(so,ir.getFieldRef())).remove(s1); // We remove null from true branch
        				}
        			}
    			}
    			else{
    				for(SymObj so: out.getPointstoSet(ptr)){
    					// Iterate throught points to set of base pointer
        				if(!(so.equals(SymObj.Null()))){
        					SymObj s1 = SymObj.Null(); // Create collection for null object
        					Collection<SymObj> s2 = new HashSet<SymObj>();
        					s2.add(s1);
        					if(out.getPointstoSet(new Pointer(so,ir.getFieldRef())) == null){
        						// If fieldref points to empty,we add a new object
    							SymObj nso = new SymObj();
    							nso.setId(hm.get(stm.toString() + tag.toString()));
    							s2.add(nso);
    						}
        					s2.addAll(out.getPointstoSet(ptr));
        					bout.setPointstoSet(new Pointer(so,ir.getFieldRef()),s2);  // We add null to branchout
        					if(fout.getPointstoSet(new Pointer(so,ir.getFieldRef())) == null ||( fout.getPointstoSet(new Pointer(so,ir.getFieldRef())).size() == 1 && fout.getPointstoSet(new Pointer(so,ir.getFieldRef())).contains(SymObj.Null()))){
        						// If points to set is empty or contains null only,then we add new object
        						SymObj nso = new SymObj();
    							nso.setId(hm.get(stm.toString() + tag.toString()));
    							Collection<SymObj> s3 = new HashSet<SymObj>();
            					s3.add(nso);
    							fout.setPointstoSet(new Pointer(so,ir.getFieldRef()), s3);
    						}
        					fout.getPointstoSet(new Pointer(so,ir.getFieldRef())).remove(s1); // We remove null from true branch
        				}
    				}
    			}
    		}
		}
		else if((right instanceof NullConstant)&&
			(left instanceof FieldRef)
			){
			InstanceFieldRef ir = (InstanceFieldRef) left; // Typecast the left operator to InstanceFieldRef
			Pointer ptr = new Pointer(ir.getBase());// Gets pointer for base of fieldref

    		if(containsNull(out,ptr)){
    			// If base pointer points to null,reports the statement as buggy statement
    			System.err.println("Store this statement3" +  tag.toString() + " " + stm.toString());
    			bugs.add(Integer.valueOf(tag.toString()));
    		}
    		if(!(out.getPointstoSet(ptr) == null)){

    			// If base pointer points to either single object or two objects but contains null
    			if(out.getPointstoSet(ptr).size() == 1 ||(out.getPointstoSet(ptr).size() == 2
        				&& containsNull(out,ptr))){
        			for(SymObj so: out.getPointstoSet(ptr)){
        				// Iterate throught points to set of base pointer
        				if(!(so.equals(SymObj.Null()))){
        					clearPointer(bout, new Pointer(so,ir.getFieldRef())); //Clears points to set of field for strong update
        					SymObj s1 = SymObj.Null();
        					Collection<SymObj> s2 = new HashSet<SymObj>();
        					s2.add(s1);
        					bout.setPointstoSet(new Pointer(so,ir.getFieldRef()),s2); // For false condition,we make strong updation such that field pointer points to null only
        					if(fout.getPointstoSet(new Pointer(so,ir.getFieldRef())) == null ||( fout.getPointstoSet(new Pointer(so,ir.getFieldRef())).size() == 1 && fout.getPointstoSet(new Pointer(so,ir.getFieldRef())).contains(SymObj.Null()))){
        						// If points to set is empty or contains null only,then we add new object
        						SymObj nso = new SymObj();
    							nso.setId(hm.get(stm.toString() + tag.toString()));
    							Collection<SymObj> s3 = new HashSet<SymObj>();
            					s3.add(nso);
    							fout.setPointstoSet(new Pointer(so,ir.getFieldRef()), s3);
    						}
        					fout.getPointstoSet(new Pointer(so,ir.getFieldRef())).remove(s1); // We remove null from true branch
        				}
        			}
    			}
    			else{
    				for(SymObj so: out.getPointstoSet(ptr)){
    					// Iterate throught points to set of base pointer
        				if(!(so.equals(SymObj.Null()))){
        					SymObj s1 = SymObj.Null(); // Create collection for null object
        					Collection<SymObj> s2 = new HashSet<SymObj>();
        					s2.add(s1);
        					if(out.getPointstoSet(new Pointer(so,ir.getFieldRef())) == null){
        						// If fieldref points to empty,we add a new object
    							SymObj nso = new SymObj();
    							nso.setId(hm.get(stm.toString() + tag.toString()));
    							s2.add(nso);
    						}
        					s2.addAll(out.getPointstoSet(ptr));
        					bout.setPointstoSet(new Pointer(so,ir.getFieldRef()),s2);
        					if(fout.getPointstoSet(new Pointer(so,ir.getFieldRef())) == null ||( fout.getPointstoSet(new Pointer(so,ir.getFieldRef())).size() == 1 && fout.getPointstoSet(new Pointer(so,ir.getFieldRef())).contains(SymObj.Null()))){
        						// If points to set is empty or contains null only,then we add new object
        						SymObj nso = new SymObj();
    							nso.setId(hm.get(stm.toString() + tag.toString()));
    							Collection<SymObj> s3 = new HashSet<SymObj>();
            					s3.add(nso);
    							fout.setPointstoSet(new Pointer(so,ir.getFieldRef()), s3);
    						}
        					fout.getPointstoSet(new Pointer(so,ir.getFieldRef())).remove(s1); // We remove null from true branch
        				}
    				}
    			}
    		}
		}
		else{
		}
	    }
	    else{
	    }
		if(stm.fallsThrough()){
			mergeWithPointstoGraphs(fallOut,bout);
			System.out.print("  Fall Outset is ---> ");
        	bout.disp();
        	System.out.println();
		}
		if(stm.branches()){
			mergeWithPointstoGraphs(branchOuts,fout);
			System.out.print("  Branch Outset is ---> ");
        	fout.disp();
        	System.out.println();
		}
	}
	else{
		if(stm.fallsThrough()){
				mergeWithPointstoGraphs(fallOut,out);
			}
			if(stm.branches()){
				mergeWithPointstoGraphs(branchOuts,out);
			}
	}

	}


    /**
     * Returns an empty points-to graph with the valid flag set to true
     */
    protected PointstoGraph entryInitialFlow() {
	PointstoGraph pg = new PointstoGraph();
	pg.setValid();
	return pg; //this is the initial value
    }

    /**
     * Return an empty points-to graph. (Valid flag is set to false by default)
     */
    protected PointstoGraph newInitialFlow() {
	return new PointstoGraph(); //is invalid initially
    }

    /**
     * overwrites dest points-to graph with source points-to graph
     */
    protected void copy(PointstoGraph source, PointstoGraph dest) {


    	dest.clear();
        dest.merge(source.clone());

    }

    /**
     * Join of two points-to graphs.
     */
    protected void merge(PointstoGraph in1, PointstoGraph in2, PointstoGraph out) {

		out.merge(in1.clone());
		out.merge(in2.clone());

    }


    /**
     * prints all the statements that are found to have a Null dereference bug
     */
    public void printBuggyStatements() {

    }
    /**
     * Gets pointer for jimple local or arraybase or instanceref
     */
	private Pointer getPointerForRef(Value i){
		System.out.println("getting Pointer  ->");
		if(i instanceof ArrayRef){
			return new Pointer(((ArrayRef) i).getBase());
    	}
		if(i instanceof InstanceFieldRef){
			System.out.println("getting InstanceFieldRef ptr ->");
			return new Pointer(((InstanceFieldRef) i).getBase());
		}
    	else
			return new Pointer(i);
	}
	/**
     * Clears points to set for strong update
     */
    private void clearPointer(PointstoGraph g,Pointer p){
    	for(PointerMap map : g.getPointerMaps()){
			if(map.getPointer().equals(p)){

				map.getMapsToSet().clear();
			}
		}
    }
    /**
     * Checks whether pointer points to null or not
     */
    private boolean containsNull(PointstoGraph g,Pointer p){
    	if(g.getPointstoSet(p) == null) return true;
    	for(SymObj j :g.getPointstoSet(p)){
    		if(j.equals(SymObj.Null())) return true;
    	}
    	return false;
    }
    private void mergeWithPointstoGraphs(List<PointstoGraph> lpts,PointstoGraph g){
		for(PointstoGraph pt: lpts){
       						pt.merge(g);

	    }
    }
    /**
     * Performs strong update on pointer ptr to point to symobjs.
     */
    private void strongUpdate(PointstoGraph pg,Pointer ptr,Collection<SymObj> symbs){
    	pg.setPointstoSet(ptr, symbs);
    }

}
package Deliverables;
import java.util.Collection;
import java.util.HashSet;


public class PointerMap {
	
	/**
	 * Pointer could be an accesspath or a reference 
	 */
	Pointer ptr;
	
	/**
	 * the set of symbolic object to which the pointer points-to
	 */
	Collection<SymObj> mapsto= new HashSet<SymObj>();
	
	public PointerMap(Pointer p, Collection<SymObj> col)
	{
		setPtr(p);
		setMapsTo(col);
	}
	
	void setPtr(Pointer p)
	{
		ptr = p;
	}		
	
	/**
	 * Sets the points-to set of this pointer
	 * @param col a non null collection of symbolic objects
	 */
	void setMapsTo(Collection<SymObj> col)
	{
		assert(col != null);
		mapsto = col;
	}
	
	/**
	 * Adds the symbolic object to the points-to set of this pointer
	 * @param o
	 */
	void addObjects(SymObj o)
	{
		mapsto.add(o);
	}
	
	/**
	 * Returns the points-to set of this pointer
	 * @return 
	 */
	Collection<SymObj> getPointstoSet()
	{
		return mapsto;
	}
	
	Pointer getPointer()
	{
		return ptr;
	}
	
	/**
	 * Same as the getPointstoSet. Use any one of them.
	 * @return
	 */
	public Collection<SymObj> getMapsToSet() {
		return mapsto;
	}
	
	public boolean equals(Object o)
	{
		PointerMap pm = (PointerMap)o;
		if( ptr.equals(pm.getPointer()) && mapsto.equals(pm.getPointstoSet()))
			return true;
		return false;	
	}
	/**
	 * Hashcode overriden so that this could be used in a hashset
	 */
	public int hashCode()
	{
		return 3;
	}
	
	/**
	 * Removes null from the points-to set if it contains null. Otherwise does nothing
	 */
	public void removeNUll() {
		
		mapsto.remove(SymObj.Null());			
	}
	
	public PointerMap clone()
	{
		/**
		 * Does not clone the object. Clones only the map
		 */
		Collection<SymObj> cl = new HashSet<SymObj>();
		for(SymObj o : mapsto)
			cl.add(o);
		
		return new PointerMap(ptr.clone(),cl);
	}	
}

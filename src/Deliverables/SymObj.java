package Deliverables;
public class SymObj
{
	int id=-1;

	private static SymObj nullPtr = new SymObj();
	
	/**
	 * returns a special symbolic object that represents the Null object. 
	 * Always use this method to create null object 
	 * @return
	 */
	public static SymObj Null()
	{
		return nullPtr;
	}
	
	/**
	 * @param i a non negative id for this object 
	 */
	public void setId(int i)
	{
		assert(id > 0);
		id = i;	
	}
	
	/**
	 * Returns the symbolic objects unique id
	 * @return
	 */
	public int getId()
	{
		return id;
	}
	
	public boolean equals(Object o)
	{
		return equiTo((SymObj)o);
	}
	
	/**
	 * Overriden so that symobj Can be used in a HashSet
	 */
	public int hashCode()
	{
		return 1;
	}

	/**
	 * Same as equals method
	 * @param o
	 * @return
	 */
	public boolean equiTo(SymObj o)
	{
		if( (this == o) || (id == o.id))
			return true;
		return false;
	}
}